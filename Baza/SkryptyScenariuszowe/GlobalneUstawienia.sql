#Wstaw stopnie wykonania zadań
insert into stopnie (ID,Stopień)
values (1, "Do zrobienia");
insert into stopnie (ID,Stopień)
values (2, "W toku");
insert into stopnie (ID,Stopień)
values (3, "Testowane");
insert into stopnie (ID,Stopień)
values (4, "Zakończone");

#Wstaw specjalizacje 
insert into specjalizacje(ID, Język)
values(1, "C++");
insert into specjalizacje(ID, Język)
values(2, "Bazy Danych");
insert into specjalizacje(ID, Język)
values(3, "Testy");
insert into specjalizacje(ID, Język)
values(4, "GUI");

#Wstaw Pracowników
insert into pracownicy(ID, ID_specjalizacji,Data_zatrudnienia)
values(1,1,"01-01-2010");
insert into pracownicy(ID, ID_specjalizacji,Data_zatrudnienia)
values(2,2,"01-10-2008");
insert into pracownicy(ID, ID_specjalizacji,Data_zatrudnienia)
values(3,3,"01-07-2014");
insert into pracownicy(ID, ID_specjalizacji,Data_zatrudnienia)
values(4,4,"01-01-2015");
insert into pracownicy(ID, ID_specjalizacji,Data_zatrudnienia)
values(5,1,"01-07-2013");
insert into pracownicy(ID, ID_specjalizacji,Data_zatrudnienia)
values(6,3,"01-06-2012");




