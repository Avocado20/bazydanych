package Database;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the userstory database table.
 * 
 */
@Entity
@Table(name="userstory")
public class Userstory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	//bi-directional many-to-one association to Iteracje
    @ManyToOne
	@JoinColumn(name="ID_iteracji")
	private Iteracje iteracje;

	//bi-directional many-to-one association to Zadania
	@OneToMany(mappedBy="userstory")
	private Set<Zadania> zadanias;

    public Userstory() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Iteracje getIteracje() {
		return this.iteracje;
	}

	public void setIteracje(Iteracje iteracje) {
		this.iteracje = iteracje;
	}
	
	public Set<Zadania> getZadanias() {
		return this.zadanias;
	}

	public void setZadanias(Set<Zadania> zadanias) {
		this.zadanias = zadanias;
	}
	
}