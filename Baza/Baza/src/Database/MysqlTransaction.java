package Database;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class MysqlTransaction {
	private  SessionFactory sessionFactory; 
	private Session session;
	public MysqlTransaction(){
		try {
			this.sessionFactory =  new AnnotationConfiguration().configure().buildSessionFactory();
			this.session = sessionFactory.openSession();
			this.session.beginTransaction();

        } catch (HibernateException ex) {
        	System.out.println("Brak połączenia z bazą");
        	System.exit(1);
        }
		
	}
	public void finalizeSession(){
		this.session.getTransaction().commit();
	}
	public void save(Object object){
		this.session.save(object);
	}
	public Session getSession(){
		return this.session;
	}
	
}
