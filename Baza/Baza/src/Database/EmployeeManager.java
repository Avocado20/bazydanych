package Database;
import java.util.ArrayList;
import java.util.List;

import Database.Pracownicy;

public class EmployeeManager {
	private Pracownicy prac;
	public EmployeeManager(int ID){
		MysqlTransaction transaction = new MysqlTransaction();
		this.prac = (Pracownicy) transaction.getSession().get(Pracownicy.class,ID);
	}
	public void setTasksAsMine(ArrayList<Zadania> tasks){
		MysqlTransaction transaction = new MysqlTransaction();
		List<Zadania> allTasks =transaction.getSession().createQuery("FROM Zadania").list();
		
		for(Zadania task :allTasks)
			if(task.getPracownicy()==null && tasks.contains(task))
				task.setPracownicy(this.prac);
	}
}
