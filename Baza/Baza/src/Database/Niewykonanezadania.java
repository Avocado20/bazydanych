package Database;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the niewykonanezadania database table.
 * 
 */
@Entity
@Table(name="niewykonanezadania")
public class Niewykonanezadania implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", insertable=true)
	private int id;

	@Column(name="Status")
	private String status;

	//bi-directional many-to-one association to Pracownicy
    @ManyToOne
	@JoinColumn(name="ID_pracownika",insertable = true, updatable=true)
	private Pracownicy pracownicy;

	//bi-directional many-to-one association to Zadania
    @ManyToOne
	@JoinColumn(name="ID_zadania",insertable = true, updatable=true)
	private Zadania zadania;

    public Niewykonanezadania() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Pracownicy getPracownicy() {
		return this.pracownicy;
	}

	public void setPracownicy(Pracownicy pracownicy) {
		this.pracownicy = pracownicy;
	}
	
	public Zadania getZadania() {
		return this.zadania;
	}

	public void setZadania(Zadania zadania) {
		this.zadania = zadania;
	}
	
}