package Database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import Database.Pracownicy;

import java.util.Date;

public class BeforeIterationLogs {
	private MysqlTransaction transaction;
	public void CreateIterationLogs() {
		this.transaction = new MysqlTransaction();
		this.CreateIteration();
		this.setDayOff();
		transaction.finalizeSession();
	}
	private void CreateIteration(){
		Iteracje iteration = new Iteracje();
		iteration.setId(1);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date dataBegin = new Date();
		Date dataEnd = new Date();
		try {
			dataBegin = formatter.parse("28-12-2015");
			dataEnd = formatter.parse("01-01-2016");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		iteration.setPoczatek(dataBegin);iteration.setKoniec(dataEnd);
		this.transaction.save(iteration);	
	}
	private void setDayOff(){
		Dniwolne first = new Dniwolne();
		first.setId(1);
		Date data = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			data = formatter.parse("01-01-2016");
			first.setData(data);
			this.transaction.save(first);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}
