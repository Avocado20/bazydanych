package Database;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import org.hibernate.Query;

public class Main extends JFrame {
	private JButton ok = new JButton("Zaloguj");
	private JButton setTasksToDo = new JButton("Przydziel wybrane zadania");
	private JButton setNewStateOfTasks = new JButton("Akceptuj zmiany");
	private JLabel ID = new JLabel();
	private JCheckBox areYouEmployee = new JCheckBox("Jestem Pracownikiem");
	private JCheckBox editEmployeeTasks = new JCheckBox("Edytuj zadania");
	
	private ArrayList<JCheckBox> tasksWhichEmployeeWants  = new ArrayList<JCheckBox>();
	private JCheckBox areYouScrumMaster = new JCheckBox("Jestem ScrumMasterem");
	private JFormattedTextField userID = new JFormattedTextField();
	public static Main logging = new Main();
	public static JFrame scrumMasterFrame = new JFrame("Panel Scrum Mastera");
	public static JFrame employeeFrame = new JFrame("Panel wyboru zadań dla pracownika");
	public static JFrame taskEditingFrame = new JFrame("Panel aktualizacji stanu zadań");
	public static JPanel taskEditingPanel = new JPanel();
	public static JFrame showStateOfTasks = new JFrame("Obecne zadania");
	public static JPanel scrumMasterPanel = new JPanel();
	public static JPanel employeeChooseTasksPanel = new JPanel();
	public static JFrame taskDeleteFrame = new JFrame("Panel usuwania zadań");
	
	public static JPanel taskDeletePanel = new JPanel();
	
	private JLabel beginOfIteration = new JLabel();
	private JLabel endOfIteration  = new JLabel();
	private JLabel addNewTask  = new JLabel("Dodaj nowe zadanie");
	private JLabel chosseTasksToDo  = new JLabel("Wybierz zadanie do wykonania");
	private JButton showTasks = new JButton("Pokaz Zadania");
	private ArrayList<JCheckBox> tasksToDo = new ArrayList<JCheckBox>();
	private JTextField taskDescription = new JTextField();
	private JComboBox typesOfTasks = new JComboBox();
	private JFormattedTextField taskDeadline = new JFormattedTextField();
	private JComboBox userStory = new JComboBox();
	private JLabel taskDescriptionLabel  = new JLabel("Opis zadania");
	private JLabel definitionOfDone  = new JLabel("Stan wykonania");
	public ScrumMaster getScrumMaster() {
		return scrumMaster;
	}
	private JLabel typeOfTaskLabel  = new JLabel("Typ zadania");
	private JLabel taskDeadlinelabel  = new JLabel("Termin ukończenia");
	private JLabel IDPracLabel  = new JLabel("ID Pracownika");
	private JLabel levelOfCompletelabel  = new JLabel("Stan Zadania");
	private JLabel userStorylabel  = new JLabel("User Story");
	private JLabel userStoryLabel  = new JLabel("UserStory");
	private JLabel timeofWorkLabel  = new JLabel("Czas Pracy");
	private JLabel taskSpecLabel  = new JLabel("Opis zadania");
	private JComboBox taskTimeOfWork = new JComboBox();
	private JLabel taskTimeOfWorkLabel= new JLabel("Czas pracy");
	private JButton addTaskButton = new JButton("Dodaj nowe zadanie");
	private ScrumMaster scrumMaster = new ScrumMaster();
	private EmployeeModerator employee;
	private ArrayList<Zadania> tasks = new ArrayList<Zadania>();
	private MysqlTransaction transactionSetTasks;
	private ArrayList<JComboBox> tasksChoose = new ArrayList<JComboBox>();
	private JComboBox taskToDelete = new JComboBox();
	private JButton deleteTask = new JButton("Panel usuwania");
	private JButton forceDeleteTask = new JButton("Usuń zadanie");
	private ArrayList<Zadania> listOfTasks;
	private ArrayList<String> taskDescriptions;
	private MysqlTransaction transactionOfDelete;
	public Main() {
			//executeServerFunctions();
		 initUI();
    }
    private void loggingSetBounds(){
    	int dlugoscTextFielda = 200;
    	beginOfIteration.setSize(20,20);
    	beginOfIteration.setBounds(20, 30, 500, 50);
    	
    	editEmployeeTasks.setSize(20,20);
    	editEmployeeTasks.setBounds(20, 20, 200, 50);
    	
    	endOfIteration.setSize(20,20);
    	endOfIteration.setBounds(20, 30, 500, 70);
    	ok.setBounds(50, 150, 160, 25);
        areYouEmployee.setSize(50,20);
        areYouEmployee.setBounds(150, 50, 300, 50);
        areYouScrumMaster.setSize(50,20);
        areYouScrumMaster.setBounds(150, 100, 500, 50);
        ID.setText("ID Pracownika");
        ID.setSize(30,50);
        ID.setBounds(50, 80, 100, 20);
        userID.setSize(20, 50);
        userID.setBounds(50, 100, 100, 20);
        ID.setText("ID Pracownika");
        taskDescription.setSize(200,100);
        taskDescriptionLabel.setBounds(0, 60, dlugoscTextFielda, 30);
        taskDescription.setBounds(0,80,dlugoscTextFielda,30);
        typesOfTasks.setSize(50,30);
        typeOfTaskLabel.setBounds(dlugoscTextFielda, 60, dlugoscTextFielda, 30);
        typesOfTasks.setBounds(dlugoscTextFielda, 80, dlugoscTextFielda, 30);
        taskDeadline.setSize(20,100);
        taskDeadlinelabel.setBounds(2*dlugoscTextFielda, 60, dlugoscTextFielda, 30);
        taskDeadline.setBounds(2*dlugoscTextFielda, 80, dlugoscTextFielda, 30);
        userStory.setSize(200,100);
        userStoryLabel.setBounds(3*dlugoscTextFielda, 60, dlugoscTextFielda, 30);
        userStory.setBounds(3*dlugoscTextFielda, 80, dlugoscTextFielda, 30);
        taskTimeOfWork.setSize(200,100);
        taskTimeOfWork.setBounds(4*dlugoscTextFielda, 80, dlugoscTextFielda, 30);
        taskTimeOfWorkLabel.setBounds(4*dlugoscTextFielda, 60, dlugoscTextFielda, 30);
        addTaskButton.setSize(200,100);
        addTaskButton.setBounds(3*dlugoscTextFielda, dlugoscTextFielda, dlugoscTextFielda, 30);
        showTasks.setSize(200,100);
        showTasks.setBounds(3*dlugoscTextFielda, 3*dlugoscTextFielda/2, dlugoscTextFielda, 30);
        setTasksToDo.setSize(200,100);
        deleteTask.setBounds(300, 400, dlugoscTextFielda/2, 30);
        deleteTask.setSize(200,40);
        taskToDelete.setBounds(20, 600, dlugoscTextFielda/2, 30);
        taskToDelete.setSize(200,100);
        
        MaskFormatter dateFormatter = null;
		try {
			dateFormatter = new MaskFormatter("##--##--####");
		} catch (ParseException e) {
			e.printStackTrace();
		}
        dateFormatter.setValidCharacters("0123456789AP");
        dateFormatter.setPlaceholderCharacter('_');     
        dateFormatter.setValueClass(String.class);   
        DefaultFormatterFactory dateFormatterFactory = new
                 DefaultFormatterFactory(dateFormatter);
        taskDeadline.setFormatterFactory(dateFormatterFactory);
        
    }
    public final void initUI() {

        setLayout(null);
        loggingSetBounds();

        add(ID);
        add(userID);
        add(ok);
        add(areYouEmployee);
        add(areYouScrumMaster);
        add(editEmployeeTasks);
        
           
        areYouEmployee.addActionListener(new loggingPanelActionListener()); 
        areYouScrumMaster.addActionListener(new loggingPanelActionListener()); 
        ok.addActionListener(new loggingPanelActionListener());
        addTaskButton.addActionListener(new loggingPanelActionListener()); 
        showTasks.addActionListener(new loggingPanelActionListener());
        setTasksToDo.addActionListener(new loggingPanelActionListener());
        setNewStateOfTasks.addActionListener(new loggingPanelActionListener());
        deleteTask.addActionListener(new loggingPanelActionListener());
        forceDeleteTask.addActionListener(new loggingPanelActionListener());
        setTitle("Logowanie do Bazy");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                logging.setVisible(true);
            }
        });
    }
    private void addTaskByScrumMaster(){
		 MysqlTransaction transaction = new MysqlTransaction();
		 Date currentDate = new Date();
		 Zadania task = new Zadania();
		 task.setOpis(taskDescription.getText());
		 SimpleDateFormat formatter = new SimpleDateFormat("dd--MM--yyyy");
		 formatter.setLenient(false);
		 Date date = new Date();
		 try {
			 	date = formatter.parse(taskDeadline.getText());
			 	Stopnie stop = new Stopnie();
				formatter.setLenient(false);
				if(currentDate.after(date)){
					System.out.println("Zadanie kończy się w przeszłości");
				}
				else{
				stop.setId(37);
				task.setStopnie(stop);
				task.setTermin_ukonczenia(date);
				task.setCzas_pracy(Integer.parseInt(taskTimeOfWork.getSelectedItem().toString()));
				task.setSpecjalizacje(this.scrumMaster.getSpecjalizacjebyIndex(typesOfTasks.getSelectedItem().toString()));
				task.setUserstory(this.scrumMaster.getUserStorybyIndex((userStory.getSelectedIndex())+1));
				transaction.save(task);
				}
		 }catch(ParseException e) {
			System.out.println("Błędna data. Zadanie nie zostanie dodane.");
		 }
		
		 transaction.finalizeSession();

	 }
 class loggingPanelActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            if(areYouEmployee.isSelected() && areYouScrumMaster.isSelected()){
            	areYouScrumMaster.setSelected(false);
            	areYouEmployee.setSelected(false);
            }
            if(areYouScrumMaster.isSelected() && e.getActionCommand().equals("Zaloguj")){
            	initScrumMasterPanel();
            }
            if(areYouEmployee.isSelected() && e.getActionCommand().equals("Zaloguj") && userID.getText().isEmpty()==false && editEmployeeTasks.isSelected()){
            	initEditingPanel();
            }else if(areYouEmployee.isSelected() && e.getActionCommand().equals("Zaloguj") && userID.getText().isEmpty()==false){
            	initEmployeePanel();
            }
            if(e.getActionCommand().equals("Dodaj nowe zadanie")){
            	addTaskByScrumMaster();
            }
            if(e.getActionCommand().equals("Pokaz Zadania")){
            	try {
            			showTasksPanel();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
            }
            if(e.getActionCommand().equals("Przydziel wybrane zadania")){
            	setTasksAsMine();
            }
            if(e.getActionCommand().equals("Akceptuj zmiany")){
            	changeStateOfTasks();
            }
            if(e.getActionCommand().equals("Panel usuwania")){
            	initDeletePanel();
            }
            if(e.getActionCommand().equals("Usuń zadanie")){
            	deleteSelectedTask();
            }
        }
        private void deleteSelectedTask(){
        	transactionOfDelete.getSession().delete(listOfTasks.get(taskToDelete.getSelectedIndex()));
        	transactionOfDelete.finalizeSession();
        	System.out.println("Usunięte");
        	System.exit(1);
        }
        private void initDeletePanel(){
        	taskDeletePanel.add(taskToDelete);
        	taskDeletePanel.add(forceDeleteTask);
        	transactionOfDelete = new MysqlTransaction();
        	listOfTasks = (ArrayList<Zadania>)transactionOfDelete.getSession().createCriteria(Zadania.class).list();
        	taskDescriptions = new ArrayList<String>();
        	for(Zadania i : listOfTasks){
        		taskDescriptions.add(i.getOpis());
        	}
        	taskToDelete.setModel(new DefaultComboBoxModel(taskDescriptions.toArray()));
        	
        	taskDeleteFrame.add(taskDeletePanel);
        	taskDeleteFrame.setSize(600,200);
        	taskDeleteFrame.setLocationRelativeTo(null);
        	taskDeleteFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	taskDeleteFrame.setVisible(true);
        }
        private void showTasksPanel() throws IOException {
        	ArrayList<Zadania> tasks = new ArrayList<Zadania>();
        	MysqlTransaction transaction = new MysqlTransaction();
        	tasks = (ArrayList<Zadania>)transaction.getSession().createCriteria(Zadania.class).list();
        	transaction.finalizeSession();
        	showStateOfTasks = new JFrame("Obecne zadania");
			showStateOfTasks.setLayout(new GridLayout(tasks.size()+1,7));
			showStateOfTasks.setBounds(200,200, 1400, 1000);
			
			showStateOfTasks.add(taskSpecLabel);
			showStateOfTasks.add(typeOfTaskLabel);
			showStateOfTasks.add(taskDeadlinelabel);
			showStateOfTasks.add(timeofWorkLabel);
			showStateOfTasks.add(IDPracLabel);
			showStateOfTasks.add(levelOfCompletelabel);
			showStateOfTasks.add(userStorylabel);
			try{
			for(Zadania i : tasks){
				showStateOfTasks.add(new JTextField(i.getOpis()));
				showStateOfTasks.add(new JTextField(i.getSpecjalizacje().getLanguage()));
				showStateOfTasks.add(new JTextField(i.getTermin_ukonczenia().toString()));
				showStateOfTasks.add(new JTextField(String.valueOf(i.getCzas_pracy())));
				try{
					showStateOfTasks.add(new JTextField(String.valueOf(i.getPracownicy().getId())));
				}
				catch(Exception e){
					showStateOfTasks.add(new JTextField("Brak Pracownika"));
				}
				showStateOfTasks.add(new JTextField(i.getStopnie().getStopien()));
				showStateOfTasks.add(new JTextField(String.valueOf(i.getUserstory().getId())));
			}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			showStateOfTasks.pack();
			showStateOfTasks.setVisible(true);
			
		}
		private void initScrumMasterPanel(){
    		System.out.println("Logowanie jako ScrumMaster");
         	logging.setVisible(false);
         	scrumMasterFrame.add(addNewTask);
         	scrumMasterFrame.add(beginOfIteration);
         	scrumMasterFrame.add(endOfIteration);
         	scrumMasterFrame.add(taskDescription);
         	scrumMasterFrame.add(taskDescriptionLabel);
         	scrumMasterFrame.add(taskDeadline);
         	scrumMasterFrame.add(taskDeadlinelabel);
         	scrumMasterFrame.add(userStory);
         	scrumMasterFrame.add(userStoryLabel);
         	scrumMasterFrame.add(taskTimeOfWork);
         	scrumMasterFrame.add(showTasks);
         	userStory.setModel(new DefaultComboBoxModel(scrumMaster.getUserStories().toArray()));	
         	typesOfTasks.setModel(new DefaultComboBoxModel(scrumMaster.getSpecjalizacje().toArray()));
         	ArrayList<Integer> timeWork = new ArrayList<Integer>(){{
    			add(1);add(2);add(3);add(5);add(8);add(13);add(21);add(34);add(55);add(89);	
    		}};
         	taskTimeOfWork.setModel(new DefaultComboBoxModel(timeWork.toArray()));
         	scrumMasterFrame.add(typesOfTasks);
         	scrumMasterFrame.add(typeOfTaskLabel);
         	scrumMasterFrame.add(userStory);
         	scrumMasterFrame.add(addTaskButton);
         	scrumMasterFrame.add(deleteTask);
         	beginOfIteration.setText("Początek Iteracji: " + scrumMaster.getBeginOfIteration());
         	endOfIteration.setText(  "Koniec Iteracji:     " + scrumMaster.getEndOfIteration());
         	scrumMasterFrame.add(scrumMasterPanel);
         	scrumMasterFrame.setSize(1200,600);
         	scrumMasterFrame.setLocationRelativeTo(null);
         	scrumMasterFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         	scrumMasterFrame.setVisible(true);
    	 }
		
		 private void setTasksAsMine(){
			 for(JCheckBox i : tasksToDo){
				 if(i.isSelected())
					 tasks.get(tasksToDo.indexOf(i)).setPracownicy(employee.getPracownicy());
			 }
			 for(Zadania i : tasks)
				 transactionSetTasks.getSession().save(i);
			 transactionSetTasks.finalizeSession();
			 transactionSetTasks = null;
			 initEmployeePanel();
			 
		 }
		 
		 
    	 private void initEmployeePanel(){
    		System.out.println("Logowanie jako Pracownik");
    		employee = new EmployeeModerator(Integer.parseInt(userID.getText()));
         	logging.setVisible(false);
         	if(transactionSetTasks==null)
         		transactionSetTasks = new MysqlTransaction();
         	tasks.clear();
         	tasksToDo.clear();
         	tasks = (ArrayList<Zadania>)transactionSetTasks.getSession().createCriteria(Zadania.class).list();
         	ArrayList<Zadania> toRemove = new ArrayList<Zadania>();
         	for(Zadania i : tasks){
         		if(i.getPracownicy()!= null)
         			toRemove.add(i);
         	}
         	tasks.removeAll(toRemove);
         	employeeChooseTasksPanel.setLayout(new GridLayout(tasks.size()+2,6));
         	for(int i=1; i<tasks.size();i++)
         	employeeChooseTasksPanel.removeAll();
         	
         	employeeChooseTasksPanel.add(taskSpecLabel);
         	employeeChooseTasksPanel.add(typeOfTaskLabel);
         	employeeChooseTasksPanel.add(taskDeadlinelabel);
         	employeeChooseTasksPanel.add(userStorylabel);
         	employeeChooseTasksPanel.add(timeofWorkLabel);
         	employeeChooseTasksPanel.add(chosseTasksToDo);
         	for(Zadania i : tasks){
         		employeeChooseTasksPanel.add(new JTextField(i.getOpis()));
         		JTextField spec = new JTextField(i.getSpecjalizacje().getLanguage());
         		if(employee.getPracownicy().getSpecjalizacje().getId()==i.getSpecjalizacje().getId())
         			spec.setForeground(Color.green);
         		employeeChooseTasksPanel.add(spec);
         		employeeChooseTasksPanel.add(new JTextField(i.getTermin_ukonczenia().toString()));
         		employeeChooseTasksPanel.add(new JTextField(String.valueOf(i.getCzas_pracy())));
         		employeeChooseTasksPanel.add(new JTextField(String.valueOf(i.getUserstory().getId())));
         		JCheckBox box = new JCheckBox();
         		employeeChooseTasksPanel.add(box);
         		tasksToDo.add(box);
			}
         	for(int i=1;i!=6;i++)
         		employeeChooseTasksPanel.add(new JPanel());
         	employeeChooseTasksPanel.add(setTasksToDo);
         	employeeFrame.add(employeeChooseTasksPanel);
         	employeeFrame.pack();
         	employeeFrame.setLocationRelativeTo(null);
         	employeeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         	employeeFrame.setVisible(true);
    	 }
    	 
    	 private void initEditingPanel(){
    		System.out.println("Logowanie jako Pracownik");
     		employee = new EmployeeModerator(Integer.parseInt(userID.getText()));
          	logging.setVisible(false);
          	if(transactionSetTasks==null)
          		transactionSetTasks = new MysqlTransaction();
          	tasks.clear();
          	tasksToDo.clear();
          	tasks = (ArrayList<Zadania>)transactionSetTasks.getSession().createCriteria(Zadania.class).list();
          	ArrayList<Zadania> toRemove = new ArrayList<Zadania>();
          	for(Zadania i : tasks){
         		if(i.getPracownicy()!=null)
         			if(i.getPracownicy().getId()!=employee.getPracownicy().getId())
         				toRemove.add(i);
         	}
          	tasks.removeAll(toRemove);
          	for(int i=1; i<tasks.size();i++)
          		editEmployeeTasks.removeAll();
          	editEmployeeTasks.setLayout(new GridLayout(tasks.size()+2,6));
          	
             	
          	editEmployeeTasks.add(taskSpecLabel);
          	editEmployeeTasks.add(definitionOfDone);
          	editEmployeeTasks.add(typeOfTaskLabel);
          	editEmployeeTasks.add(taskDeadlinelabel);
          	editEmployeeTasks.add(userStorylabel);
          	editEmployeeTasks.add(timeofWorkLabel);
          	tasksChoose.clear();
             System.out.println(tasks.size());
             for(Zadania i : tasks){
            	editEmployeeTasks.add(new JTextField(i.getOpis()));
            	JComboBox comboBox = new JComboBox();
            	comboBox.setModel(new DefaultComboBoxModel(employee.getStopnie().toArray()));
            	comboBox.setSelectedItem(i.getStopnie().getStopien());
            	tasksChoose.add(comboBox);
            	editEmployeeTasks.add(comboBox);
            	editEmployeeTasks.add(new JTextField(i.getSpecjalizacje().getLanguage()));
          		editEmployeeTasks.add(new JTextField(i.getTermin_ukonczenia().toString()));
          		editEmployeeTasks.add(new JTextField(String.valueOf(i.getUserstory().getId())));
          		editEmployeeTasks.add(new JTextField(String.valueOf(i.getCzas_pracy())));
             }
             for(int i=1; i<6;i++)
            	 editEmployeeTasks.add(new JPanel());
             editEmployeeTasks.add(setNewStateOfTasks);
             employeeFrame.add(editEmployeeTasks);
             employeeFrame.pack();
             employeeFrame.setLocationRelativeTo(null);
             employeeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
             employeeFrame.setVisible(true);
    	 }
    	 private void changeStateOfTasks(){
    		 ArrayList<Stopnie> stopnie= new ArrayList<Stopnie>(employee.getStopnieS());
    		 for(Zadania i : tasks){
    			 JComboBox box = tasksChoose.get(tasks.indexOf(i));
    			 	if(box.getSelectedIndex() != stopnie.indexOf(i.getStopnie())){
    			 		i.setStopnie(stopnie.get(box.getSelectedIndex()));
    			 		transactionSetTasks.save(i);
    			 	}
			 }
			 transactionSetTasks.finalizeSession();
			 transactionSetTasks = null;
			 initEditingPanel();
		 }
     }	
 		
}