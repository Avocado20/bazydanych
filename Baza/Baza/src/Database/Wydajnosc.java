package Database;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the wydajnosc database table.
 * 
 */
@Entity
@Table(name="wydajnosc")
public class Wydajnosc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="Godziny")
	private int godziny;

	//bi-directional many-to-one association to Pracownicy
    @ManyToOne
	@JoinColumn(name="ID_pracownika")
	private Pracownicy pracownicy;

	//bi-directional many-to-one association to Umowy
    @ManyToOne
	@JoinColumn(name="ID_umowy")
	private Umowy umowy;

    public Wydajnosc() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGodziny() {
		return this.godziny;
	}

	public void setGodziny(int godziny) {
		this.godziny = godziny;
	}

	public Pracownicy getPracownicy() {
		return this.pracownicy;
	}

	public void setPracownicy(Pracownicy pracownicy) {
		this.pracownicy = pracownicy;
	}
	
	public Umowy getUmowy() {
		return this.umowy;
	}

	public void setUmowy(Umowy umowy) {
		this.umowy = umowy;
	}
	
}