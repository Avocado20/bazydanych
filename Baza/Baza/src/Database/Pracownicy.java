package Database;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the pracownicy database table.
 * 
 */
@Entity
@Table(name="pracownicy")
public class Pracownicy implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private Date data_zatrudnienia;
	private Set<Dniwolne> dniwolnes1;
	private Set<Niewykonanezadania> niewykonanezadanias;
	private Specjalizacje specjalizacje;
	private Set<Wydajnosc> wydajnoscs;
	private Set<Zadania> zadanias;

    public Pracownicy() {
    }


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", insertable = true, updatable = true)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="Data_zatrudnienia" , insertable = true, updatable = true)
	public Date getData_zatrudnienia() {
		return this.data_zatrudnienia;
	}

	public void setData_zatrudnienia(Date data_zatrudnienia) {
		this.data_zatrudnienia = data_zatrudnienia;
	}


	//bi-directional many-to-one association to Dniwolne
	@OneToMany(mappedBy="pracownicy1")
	public Set<Dniwolne> getDniwolnes1() {
		return this.dniwolnes1;
	}

	public void setDniwolnes1(Set<Dniwolne> dniwolnes1) {
		this.dniwolnes1 = dniwolnes1;
	}
	
	//bi-directional many-to-one association to Niewykonanezadania
	@OneToMany(mappedBy="pracownicy")
	public Set<Niewykonanezadania> getNiewykonanezadanias() {
		return this.niewykonanezadanias;
	}

	public void setNiewykonanezadanias(Set<Niewykonanezadania> niewykonanezadanias) {
		this.niewykonanezadanias = niewykonanezadanias;
	}
	

	//bi-directional many-to-one association to Specjalizacje
    @ManyToOne
	@JoinColumn(name="ID_specjalizacji")
	public Specjalizacje getSpecjalizacje() {
		return this.specjalizacje;
	}

	public void setSpecjalizacje(Specjalizacje specjalizacje) {
		this.specjalizacje = specjalizacje;
	}
	

	//bi-directional many-to-one association to Wydajnosc
	@OneToMany(mappedBy="pracownicy")
	public Set<Wydajnosc> getWydajnoscs() {
		return this.wydajnoscs;
	}

	public void setWydajnoscs(Set<Wydajnosc> wydajnoscs) {
		this.wydajnoscs = wydajnoscs;
	}
	

	//bi-directional many-to-one association to Zadania
	@OneToMany(mappedBy="pracownicy")
	public Set<Zadania> getZadanias() {
		return this.zadanias;
	}

	public void setZadanias(Set<Zadania> zadanias) {
		this.zadanias = zadanias;
	}
	
}