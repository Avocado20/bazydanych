package Database;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.ObjectNotFoundException;

import Database.Pracownicy;

public class EmployeeModerator {
	public Pracownicy prac;
	private ArrayList<Specjalizacje> specjalizacje = new ArrayList<Specjalizacje>();
	private ArrayList<Stopnie> stopnie = new ArrayList<Stopnie>();
	public EmployeeModerator(int ID){
		MysqlTransaction transaction = new MysqlTransaction();
		try{
			this.prac = (Pracownicy) transaction.getSession().get(Pracownicy.class,ID);
			
		}
		catch (Exception e){
			System.out.println("Nie ma takiego pracownika w bazie");
		}
		if(prac==null){
			System.out.println("Nie ma takiego pracownika w bazie, program zostanie zamknięty");
			System.exit(1);
		}
		this.specjalizacje = (ArrayList<Specjalizacje>)transaction.getSession().createCriteria(Specjalizacje.class).list();
		this.stopnie = (ArrayList<Stopnie>)transaction.getSession().createCriteria(Stopnie.class).list();
		System.out.println("Jest pracownik w Bazie");
		transaction.finalizeSession();
	}
	public ArrayList<String> getSpecjalizacje() {
		ArrayList<String> ret = new ArrayList<String>();
		for(Specjalizacje i : specjalizacje)
			ret.add(i.getLanguage().toString());
		return ret;
	}
	
	public ArrayList<String> getStopnie() {
		ArrayList<String> ret = new ArrayList<String>();
		for(Stopnie i : stopnie)
			ret.add(i.getStopien());
		return ret;
	}
	public ArrayList<Stopnie> getStopnieS() {
		return this.stopnie;
	}
	public void setTasksAsMine(ArrayList<Zadania> tasks){
		MysqlTransaction transaction = new MysqlTransaction();
		List<Zadania> allTasks =transaction.getSession().createQuery("FROM Zadania").list();
		
		for(Zadania task :allTasks)
			if(task.getPracownicy()==null && tasks.contains(task))
				task.setPracownicy(this.prac);
	}
	public Pracownicy getPracownicy(){
		return this.prac;
	}
}
