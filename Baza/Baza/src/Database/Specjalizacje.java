package Database;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the specjalizacje database table.
 * 
 */
@Entity
@Table(name="specjalizacje")
public class Specjalizacje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="Language")
	private String language;

	//bi-directional many-to-one association to Pracownicy
	@OneToMany(mappedBy="specjalizacje")
	private Set<Pracownicy> pracownicies;

	//bi-directional many-to-one association to Zadania
	@OneToMany(mappedBy="specjalizacje")
	private Set<Zadania> zadanias;

    public Specjalizacje() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Set<Pracownicy> getPracownicies() {
		return this.pracownicies;
	}

	public void setPracownicies(Set<Pracownicy> pracownicies) {
		this.pracownicies = pracownicies;
	}
	
	public Set<Zadania> getZadanias() {
		return this.zadanias;
	}

	public void setZadanias(Set<Zadania> zadanias) {
		this.zadanias = zadanias;
	}
	
}