package DarkaBazy;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Specjalizacje database table.
 * 
 */
@Entity
public class Specjalizacje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idSpecjalizacje;

	@Column(name="Atrakcyjnoscspecjalizacji")
	private int atrakcyjnoscspecjalizacji;

	@Column(name="Nazwaspecjalizacji")
	private String nazwaspecjalizacji;

    public Specjalizacje() {
    }

	public int getIdSpecjalizacje() {
		return this.idSpecjalizacje;
	}

	public void setIdSpecjalizacje(int idSpecjalizacje) {
		this.idSpecjalizacje = idSpecjalizacje;
	}

	public int getAtrakcyjnoscspecjalizacji() {
		return this.atrakcyjnoscspecjalizacji;
	}

	public void setAtrakcyjnoscspecjalizacji(int atrakcyjnoscspecjalizacji) {
		this.atrakcyjnoscspecjalizacji = atrakcyjnoscspecjalizacji;
	}

	public String getNazwaspecjalizacji() {
		return this.nazwaspecjalizacji;
	}

	public void setNazwaspecjalizacji(String nazwaspecjalizacji) {
		this.nazwaspecjalizacji = nazwaspecjalizacji;
	}

}