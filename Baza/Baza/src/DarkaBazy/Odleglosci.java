package DarkaBazy;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Odleglosci database table.
 * 
 */
@Entity
public class Odleglosci implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IdDrogi")
	private int idDrogi;

	@Column(name="Dystans")
	private int dystans;

    public Odleglosci() {
    }

	public int getIdDrogi() {
		return this.idDrogi;
	}

	public void setIdDrogi(int idDrogi) {
		this.idDrogi = idDrogi;
	}

	public int getDystans() {
		return this.dystans;
	}

	public void setDystans(int dystans) {
		this.dystans = dystans;
	}

}