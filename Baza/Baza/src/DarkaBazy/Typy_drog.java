package DarkaBazy;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Typy drog database table.
 * 
 */
@Entity
@Table(name="Typy drog")
public class Typy_drog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name=""idTypy drog"")
	private int _idTypy_drog_;

	@Column(name=""Jakosc utwardzenia"")
	private String _Jakosc_utwardzenia_;

	@Column(name=""Koszt budowy"")
	private int _Koszt_budowy_;

	@Column(name=""Nazwa typu"")
	private String _Nazwa_typu_;

	@Column(name=""wspolczynnik przemieszczanie się po drodze"")
	private int _wspolczynnik_przemieszczanie_się_po_drodze_;

    public Typy_drog() {
    }

	public int get_idTypy_drog_() {
		return this._idTypy_drog_;
	}

	public void set_idTypy_drog_(int _idTypy_drog_) {
		this._idTypy_drog_ = _idTypy_drog_;
	}

	public String get_Jakosc_utwardzenia_() {
		return this._Jakosc_utwardzenia_;
	}

	public void set_Jakosc_utwardzenia_(String _Jakosc_utwardzenia_) {
		this._Jakosc_utwardzenia_ = _Jakosc_utwardzenia_;
	}

	public int get_Koszt_budowy_() {
		return this._Koszt_budowy_;
	}

	public void set_Koszt_budowy_(int _Koszt_budowy_) {
		this._Koszt_budowy_ = _Koszt_budowy_;
	}

	public String get_Nazwa_typu_() {
		return this._Nazwa_typu_;
	}

	public void set_Nazwa_typu_(String _Nazwa_typu_) {
		this._Nazwa_typu_ = _Nazwa_typu_;
	}

	public int get_wspolczynnik_przemieszczanie_się_po_drodze_() {
		return this._wspolczynnik_przemieszczanie_się_po_drodze_;
	}

	public void set_wspolczynnik_przemieszczanie_się_po_drodze_(int _wspolczynnik_przemieszczanie_się_po_drodze_) {
		this._wspolczynnik_przemieszczanie_się_po_drodze_ = _wspolczynnik_przemieszczanie_się_po_drodze_;
	}

}