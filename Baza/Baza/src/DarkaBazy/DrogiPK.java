package DarkaBazy;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Drogi database table.
 * 
 */
@Embeddable
public class DrogiPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Osady_idMiasto")
	private int osady_idMiasto;

	@Column(name=""Typy drog_idTypy drog"")
	private int _Typy_drog_idTypy_drog_;

	@Column(name="Odleglosci_IdDrogi")
	private int odleglosci_IdDrogi;

    public DrogiPK() {
    }
	public int getOsady_idMiasto() {
		return this.osady_idMiasto;
	}
	public void setOsady_idMiasto(int osady_idMiasto) {
		this.osady_idMiasto = osady_idMiasto;
	}
	public int get_Typy_drog_idTypy_drog_() {
		return this._Typy_drog_idTypy_drog_;
	}
	public void set_Typy_drog_idTypy_drog_(int _Typy_drog_idTypy_drog_) {
		this._Typy_drog_idTypy_drog_ = _Typy_drog_idTypy_drog_;
	}
	public int getOdleglosci_IdDrogi() {
		return this.odleglosci_IdDrogi;
	}
	public void setOdleglosci_IdDrogi(int odleglosci_IdDrogi) {
		this.odleglosci_IdDrogi = odleglosci_IdDrogi;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DrogiPK)) {
			return false;
		}
		DrogiPK castOther = (DrogiPK)other;
		return 
			(this.osady_idMiasto == castOther.osady_idMiasto)
			&& (this._Typy_drog_idTypy_drog_ == castOther._Typy_drog_idTypy_drog_)
			&& (this.odleglosci_IdDrogi == castOther.odleglosci_IdDrogi);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.osady_idMiasto;
		hash = hash * prime + this._Typy_drog_idTypy_drog_;
		hash = hash * prime + this.odleglosci_IdDrogi;
		
		return hash;
    }
}