package DarkaBazy;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Jednostki database table.
 * 
 */
@Entity
public class Jednostki implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idJednostki;

	@Column(name=""Nazwa jednostki"")
	private String _Nazwa_jednostki_;

	@Column(name=""Skutecznosc w walce na dystans"")
	private int _Skutecznosc_w_walce_na_dystans_;

	@Column(name=""Skutecznosc w walce wręcz"")
	private int _Skutecznosc_w_walce_wręcz_;

    public Jednostki() {
    }

	public int getIdJednostki() {
		return this.idJednostki;
	}

	public void setIdJednostki(int idJednostki) {
		this.idJednostki = idJednostki;
	}

	public String get_Nazwa_jednostki_() {
		return this._Nazwa_jednostki_;
	}

	public void set_Nazwa_jednostki_(String _Nazwa_jednostki_) {
		this._Nazwa_jednostki_ = _Nazwa_jednostki_;
	}

	public int get_Skutecznosc_w_walce_na_dystans_() {
		return this._Skutecznosc_w_walce_na_dystans_;
	}

	public void set_Skutecznosc_w_walce_na_dystans_(int _Skutecznosc_w_walce_na_dystans_) {
		this._Skutecznosc_w_walce_na_dystans_ = _Skutecznosc_w_walce_na_dystans_;
	}

	public int get_Skutecznosc_w_walce_wręcz_() {
		return this._Skutecznosc_w_walce_wręcz_;
	}

	public void set_Skutecznosc_w_walce_wręcz_(int _Skutecznosc_w_walce_wręcz_) {
		this._Skutecznosc_w_walce_wręcz_ = _Skutecznosc_w_walce_wręcz_;
	}

}