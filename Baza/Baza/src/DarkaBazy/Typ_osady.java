package DarkaBazy;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the Typ osady database table.
 * 
 */
@Entity
@Table(name="Typ osady")
public class Typ_osady implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name=""idTyp osady"")
	private int _idTyp_osady_;

	@Column(name=""Nazwa typu"")
	private String _Nazwa_typu_;

	@Column(name=""Wartosc ekonomiczna"")
	private int _Wartosc_ekonomiczna_;

	@Column(name=""Wartosc obronna"")
	private int _Wartosc_obronna_;

	//bi-directional many-to-one association to Osady
	@OneToMany(mappedBy="typOsady")
	private Set<Osady> osadies;

    public Typ_osady() {
    }

	public int get_idTyp_osady_() {
		return this._idTyp_osady_;
	}

	public void set_idTyp_osady_(int _idTyp_osady_) {
		this._idTyp_osady_ = _idTyp_osady_;
	}

	public String get_Nazwa_typu_() {
		return this._Nazwa_typu_;
	}

	public void set_Nazwa_typu_(String _Nazwa_typu_) {
		this._Nazwa_typu_ = _Nazwa_typu_;
	}

	public int get_Wartosc_ekonomiczna_() {
		return this._Wartosc_ekonomiczna_;
	}

	public void set_Wartosc_ekonomiczna_(int _Wartosc_ekonomiczna_) {
		this._Wartosc_ekonomiczna_ = _Wartosc_ekonomiczna_;
	}

	public int get_Wartosc_obronna_() {
		return this._Wartosc_obronna_;
	}

	public void set_Wartosc_obronna_(int _Wartosc_obronna_) {
		this._Wartosc_obronna_ = _Wartosc_obronna_;
	}

	public Set<Osady> getOsadies() {
		return this.osadies;
	}

	public void setOsadies(Set<Osady> osadies) {
		this.osadies = osadies;
	}
	
}