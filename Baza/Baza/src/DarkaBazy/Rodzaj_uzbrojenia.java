package DarkaBazy;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Rodzaj uzbrojenia database table.
 * 
 */
@Entity
@Table(name="Rodzaj uzbrojenia")
public class Rodzaj_uzbrojenia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name=""idRodzaj uzbrojenia"")
	private int _idRodzaj_uzbrojenia_;

	@Column(name="Efekt")
	private int efekt;

	@Column(name=""Typ uzbrojenia"")
	private String _Typ_uzbrojenia_;

    public Rodzaj_uzbrojenia() {
    }

	public int get_idRodzaj_uzbrojenia_() {
		return this._idRodzaj_uzbrojenia_;
	}

	public void set_idRodzaj_uzbrojenia_(int _idRodzaj_uzbrojenia_) {
		this._idRodzaj_uzbrojenia_ = _idRodzaj_uzbrojenia_;
	}

	public int getEfekt() {
		return this.efekt;
	}

	public void setEfekt(int efekt) {
		this.efekt = efekt;
	}

	public String get_Typ_uzbrojenia_() {
		return this._Typ_uzbrojenia_;
	}

	public void set_Typ_uzbrojenia_(String _Typ_uzbrojenia_) {
		this._Typ_uzbrojenia_ = _Typ_uzbrojenia_;
	}

}