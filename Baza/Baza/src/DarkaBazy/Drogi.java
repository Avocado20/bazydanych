package DarkaBazy;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Drogi database table.
 * 
 */
@Entity
public class Drogi implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DrogiPK id;

    public Drogi() {
    }

	public DrogiPK getId() {
		return this.id;
	}

	public void setId(DrogiPK id) {
		this.id = id;
	}
	
}